cd "Triennale/TerzoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/TerzoAnno/startup-di-impresa.git "Startup di Impresa"
cd ../..

cd "Triennale/TerzoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/TerzoAnno/social-media-management.git "Social Media Management"
cd ../..

cd "Triennale/TerzoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/TerzoAnno/sistemi-centrali.git "Sistemi Centrali"
cd ../..

cd "Triennale/TerzoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/TerzoAnno/gpu.git "Programmazione Parallela su Architetture GPU"
cd ../..

cd "Triennale/TerzoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/TerzoAnno/microcontrollori.git "Laboratorio di Sistemi a Microcontrollore"
cd ../..

cd "Triennale/TerzoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/TerzoAnno/programmazione-mobile.git "Programmazione Mobile"
cd ../..

cd "Triennale/TerzoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/TerzoAnno/tecniche-di-programmazione-concorrenti-e-distribuite.git "Tecniche di programmazione concorrenti e distribuite"
cd ../..

cd "Triennale/TerzoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/TerzoAnno/informatica-musicale.git "Informatica Musicale"
cd ../..

cd "Triennale/TerzoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/TerzoAnno/metodi-matematici-e-statistici.git "Metodi Matematici e Statistici"
cd ../..

cd "Triennale/TerzoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/TerzoAnno/internet-security.git "Internet Security"
cd ../..

cd "Triennale/TerzoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/TerzoAnno/fisica.git "Fisica"
cd ../..

cd "Triennale/TerzoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/TerzoAnno/digital-game-development.git "Digital Game Development"
cd ../..

cd "Triennale/TerzoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/TerzoAnno/datamining.git "DataMining"
cd ../..

cd "Triennale/TerzoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/TerzoAnno/computer-grafica.git "Computer Grafica"
cd ../..

cd "Triennale/TerzoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/TerzoAnno/computer-forensics.git "Computer Forensics"
cd ../..

cd "Triennale/SecondoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/SecondoAnno/basi-di-dati.git "Basi di Dati"
cd ../..

cd "Triennale/SecondoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/SecondoAnno/sistemi-operativi.git "Sistemi Operativi"
cd ../..

cd "Triennale/SecondoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/SecondoAnno/reti-di-calcolatori.git "Reti di Calcolatori"
cd ../..

cd "Triennale/SecondoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/SecondoAnno/interazione-e-multimedia.git "Interazione e Multimedia"
cd ../..

cd "Triennale/SecondoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/SecondoAnno/ingegneria-del-software.git "Ingegneria del software"
cd ../..

cd "Triennale/SecondoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/SecondoAnno/algoritmi.git "Algoritmi"
cd ../..

cd "Triennale/PrimoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/PrimoAnno/matematica-discreta.git "Matematica Discreta"
cd ../..

cd "Triennale/PrimoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/PrimoAnno/programmazione-2.git "Programmazione 2"
cd ../..

cd "Triennale/PrimoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/PrimoAnno/programmazione-1.git "Programmazione 1"
cd ../..

cd "Triennale/PrimoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/PrimoAnno/corso-zero-matematica-ofa.git "Corso Zero Matematica OFA"
cd ../..

cd "Triennale/PrimoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/PrimoAnno/corso-zero-informatica.git "Corso Zero Informatica"
cd ../..

cd "Triennale/PrimoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/PrimoAnno/elementi-di-analisi-matematica-1.git "Elementi di Analisi Matematica 1"
cd ../..

cd "Triennale/PrimoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/PrimoAnno/fondamenti-di-informatica.git "Fondamenti di Informatica"
cd ../..

cd "Triennale/PrimoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/PrimoAnno/architettura-degli-elaboratori.git "Architettura degli Elaboratori"
cd ../..

cd "Triennale/TerzoAnno"
git clone git@gitlab.com:UNICT-DMI/Triennale/TerzoAnno/technologies-for-advanced-programming.git "Technologies for Advanced Programming"
cd ../..

cd "Magistrale/Primo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/PrimoSemestre/ingegneria-dei-sistemi-distribuiti.git "Ingegneria dei sistemi distribuiti"
cd ../..

cd "Magistrale/Primo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/PrimoSemestre/cryptographic-engineering.git "Cryptographic Engineering"
cd ../..

cd "Magistrale/Primo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/PrimoSemestre/sistemi-dedicati.git "Sistemi Dedicati"
cd ../..

cd "Magistrale/Primo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/PrimoSemestre/peer-to-peer.git "Peer to Peer"
cd ../..

cd "Magistrale/Primo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/PrimoSemestre/linguaggi-formali.git "Linguaggi Formali"
cd ../..

cd "Magistrale/Primo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/PrimoSemestre/bioinformatica.git "Bioinformatica"
cd ../..

cd "Magistrale/Primo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/PrimoSemestre/multimedia.git "Multimedia"
cd ../..

cd "Magistrale/Primo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/PrimoSemestre/ottimizzazione.git "Ottimizzazione"
cd ../..

cd "Magistrale/Primo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/PrimoSemestre/fondamenti-di-analisi-dei-dati.git "Fondamenti di Analisi dei Dati"
cd ../..

cd "Magistrale/Primo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/PrimoSemestre/crittografia.git "Crittografia"
cd ../..

cd "Magistrale/Primo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/PrimoSemestre/algoritmi.git "Algoritmi e complessita"
cd ../..

cd "Magistrale/Secondo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/SecondoSemestre/web-reasoning.git "Web Reasoning"
cd ../..

cd "Magistrale/Secondo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/SecondoSemestre/sistemi-distribuiti-2.git "Sistemi Distribuiti 2"
cd ../..

cd "Magistrale/Secondo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/SecondoSemestre/fondamenti-e-linguaggi.git "Fondamenti e Linguaggi"
cd ../..

cd "Magistrale/Secondo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/SecondoSemestre/computazione-naturale-e-bioispirata.git "Computazione naturale e bioispirata"
cd ../..

cd "Magistrale/Secondo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/SecondoSemestre/algoritmi-randomizzati-ed-approssimati.git "Algoritmi Randomizzati ed Approssimati"
cd ../..

cd "Magistrale/Secondo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/SecondoSemestre/computabilita.git "Computabilita"
cd ../..

cd "Magistrale/Secondo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/SecondoSemestre/sistemi-robotici.git "Sistemi Robotici"
cd ../..

cd "Magistrale/Secondo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/SecondoSemestre/machine-learning.git "Machine Learning"
cd ../..

cd "Magistrale/Secondo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/SecondoSemestre/computer-vision.git "Computer Vision"
cd ../..

cd "Magistrale/Secondo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/SecondoSemestre/computer-security.git "Computer Security"
cd ../..

cd "Magistrale/Secondo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/SecondoSemestre/big-data.git "Big Data"
cd ../..

cd "Magistrale/Secondo Semestre"
git clone git@gitlab.com:UNICT-DMI/Magistrale/SecondoSemestre/analisi-numerica.git "Analisi Numerica"
cd ../..
