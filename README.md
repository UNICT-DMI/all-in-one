# All-in-one

This repository contain some tools to sync, update and make LFS all large files.


### Sync all repositories

```
$ sh init_modules.sh
```

### Update all repositories

```
$ sh update_modules.sh
```

### Make all large files "LFS"

```
$ sh lfs.sh
```

This last script tag all pdf, doc, docx, ppt, pptx, jpg, jpeg, png, zip, rar files as "LFS" and push automatically the new updates.


### TODO

Make a new tool with following features:

- [ ] select what year and subjects sync
- [ ] get all the subjects (repositories) from the gitlab API
