# Coded By Helias

set_lfs() {
    git checkout master
    git pull

    rm .gitattributes

    echo "*.jpeg filter=lfs diff=lfs merge=lfs -text
*.jpg filter=lfs diff=lfs merge=lfs -text
*.ppt filter=lfs diff=lfs merge=lfs -text
*.pptx filter=lfs diff=lfs merge=lfs -text
*.docx filter=lfs diff=lfs merge=lfs -text
*.doc filter=lfs diff=lfs merge=lfs -text
*.pdf filter=lfs diff=lfs merge=lfs -text
*.png filter=lfs diff=lfs merge=lfs -text
*.zip filter=lfs diff=lfs merge=lfs -text" > .gitattributes

    git lfs track *.jpeg
    git lfs track *.jpg
    git lfs track *.ppt
    git lfs track *.pptx
    git lfs track *.docx
    git lfs track *.doc
    git lfs track *.png
    git lfs track *.zip
    git lfs track *.pdf
    git lfs track *.rar

    git lfs prune
}

recursive_lfs() {
    local i="$1"
    local d

    cd "$i";
    set_lfs # function set_lfs()

    local SUBDIR=*/

    for d in $SUBDIR; do
        if [ "$d" != "*/" ]
        then
            recursive_lfs "$d"
        fi
        # echo "$d";
    done

    cd ..
}

init_lfs() {
    cd "$1"
    X=*/ # get list of all directories
    for dir in $X; do
        cd "$dir"
        recursive_lfs "./"

        echo $dir
        cd "$dir"
        git add *
        git add .gitattributes
        git rm .gitattributes.txt
        git commit -m 'Added LFS files'
        git push
        cd ..
    done
}

init_lfs "Magistrale/Primo Semestre"
init_lfs "Magistrale/Secondo Semestre"
init_lfs "Triennale/PrimoAnno"
init_lfs "Triennale/SecondoAnno"
init_lfs "Triennale/TerzoAnno"

