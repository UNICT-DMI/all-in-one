echo "Startup di Impresa"
cd "Triennale/TerzoAnno/Startup di Impresa"
git pull
cd ../../..

echo "Social Media Management"
cd "Triennale/TerzoAnno/Social Media Management"
git pull
cd ../../..

echo "Sistemi Centrali"
cd "Triennale/TerzoAnno/Sistemi Centrali"
git pull
cd ../../..

echo "Programmazione Parallela su Architetture GPU"
cd "Triennale/TerzoAnno/Programmazione Parallela su Architetture GPU"
git pull
cd ../../..

echo "Laboratorio di Sistemi a Microcontrollore"
cd "Triennale/TerzoAnno/Laboratorio di Sistemi a Microcontrollore"
git pull
cd ../../..

echo "Programmazione Mobile"
cd "Triennale/TerzoAnno/Programmazione Mobile"
git pull
cd ../../..

echo "Tecniche di programmazione concorrenti e distribuite"
cd "Triennale/TerzoAnno/Tecniche di programmazione concorrenti e distribuite"
git pull
cd ../../..

echo "Informatica Musicale"
cd "Triennale/TerzoAnno/Informatica Musicale"
git pull
cd ../../..

echo "Metodi Matematici e Statistici"
cd "Triennale/TerzoAnno/Metodi Matematici e Statistici"
git pull
cd ../../..

echo "Internet Security"
cd "Triennale/TerzoAnno/Internet Security"
git pull
cd ../../..

echo "Fisica"
cd "Triennale/TerzoAnno/Fisica"
git pull
cd ../../..

echo "Digital Game Development"
cd "Triennale/TerzoAnno/Digital Game Development"
git pull
cd ../../..

echo "DataMining"
cd "Triennale/TerzoAnno/DataMining"
git pull
cd ../../..

echo "Computer Grafica"
cd "Triennale/TerzoAnno/Computer Grafica"
git pull
cd ../../..

echo "Computer Forensics"
cd "Triennale/TerzoAnno/Computer Forensics"
git pull
cd ../../..

echo "Basi di Dati"
cd "Triennale/SecondoAnno/Basi di Dati"
git pull
cd ../../..

echo "Sistemi Operativi"
cd "Triennale/SecondoAnno/Sistemi Operativi"
git pull
cd ../../..

echo "Reti di Calcolatori"
cd "Triennale/SecondoAnno/Reti di Calcolatori"
git pull
cd ../../..

echo "Interazione e Multimedia"
cd "Triennale/SecondoAnno/Interazione e Multimedia"
git pull
cd ../../..

echo "Ingegneria del software"
cd "Triennale/SecondoAnno/Ingegneria del software"
git pull
cd ../../..

echo "Algoritmi"
cd "Triennale/SecondoAnno/Algoritmi"
git pull
cd ../../..

echo "Matematica Discreta"
cd "Triennale/PrimoAnno/Matematica Discreta"
git pull
cd ../../..

echo "Programmazione 2"
cd "Triennale/PrimoAnno/Programmazione 2"
git pull
cd ../../..

echo "Programmazione 1"
cd "Triennale/PrimoAnno/Programmazione 1"
git pull
cd ../../..

echo "Corso Zero Matematica OFA"
cd "Triennale/PrimoAnno/Corso Zero Matematica OFA"
git pull
cd ../../..

echo "Corso Zero Informatica"
cd "Triennale/PrimoAnno/Corso Zero Informatica"
git pull
cd ../../..

echo "Ingegneria dei sistemi distribuiti"
cd "Magistrale/Primo Semestre/Ingegneria dei sistemi distribuiti"
git pull
cd ../../..

echo "Cryptographic Engineering"
cd "Magistrale/Primo Semestre/Cryptographic Engineering"
git pull
cd ../../..

echo "Sistemi Dedicati"
cd "Magistrale/Primo Semestre/Sistemi Dedicati"
git pull
cd ../../..

echo "Peer to Peer"
cd "Magistrale/Primo Semestre/Peer to Peer"
git pull
cd ../../..

echo "Linguaggi Formali"
cd "Magistrale/Primo Semestre/Linguaggi Formali"
git pull
cd ../../..

echo "Bioinformatica"
cd "Magistrale/Primo Semestre/Bioinformatica"
git pull
cd ../../..

echo "Multimedia"
cd "Magistrale/Primo Semestre/Multimedia"
git pull
cd ../../..

echo "Ottimizzazione"
cd "Magistrale/Primo Semestre/Ottimizzazione"
git pull
cd ../../..

echo "Fondamenti di Analisi dei Dati"
cd "Magistrale/Primo Semestre/Fondamenti di Analisi dei Dati"
git pull
cd ../../..

echo "Crittografia"
cd "Magistrale/Primo Semestre/Crittografia"
git pull
cd ../../..

echo "Algoritmi e complessita"
cd "Magistrale/Primo Semestre/Algoritmi e complessita"
git pull
cd ../../..

echo "Web Reasoning"
cd "Magistrale/Secondo Semestre/Web Reasoning"
git pull
cd ../../..

echo "Sistemi Distribuiti 2"
cd "Magistrale/Secondo Semestre/Sistemi Distribuiti 2"
git pull
cd ../../..

echo "Fondamenti e Linguaggi"
cd "Magistrale/Secondo Semestre/Fondamenti e Linguaggi"
git pull
cd ../../..

echo "Computazione naturale e bioispirata"
cd "Magistrale/Secondo Semestre/Computazione naturale e bioispirata"
git pull
cd ../../..

echo "Algoritmi Randomizzati ed Approssimati"
cd "Magistrale/Secondo Semestre/Algoritmi Randomizzati ed Approssimati"
git pull
cd ../../..

echo "Computabilita"
cd "Magistrale/Secondo Semestre/Computabilita"
git pull
cd ../../..

echo "Sistemi Robotici"
cd "Magistrale/Secondo Semestre/Sistemi Robotici"
git pull
cd ../../..

echo "Machine Learning"
cd "Magistrale/Secondo Semestre/Machine Learning"
git pull
cd ../../..

echo "Computer Vision"
cd "Magistrale/Secondo Semestre/Computer Vision"
git pull
cd ../../..

echo "Computer Security"
cd "Magistrale/Secondo Semestre/Computer Security"
git pull
cd ../../..

echo "Big Data"
cd "Magistrale/Secondo Semestre/Big Data"
git pull
cd ../../..

echo "Analisi Numerica"
cd "Magistrale/Secondo Semestre/Analisi Numerica"
git pull
cd ../../..

echo "Elementi di Analisi Matematica 1"
cd "Triennale/PrimoAnno/Elementi di Analisi Matematica 1"
git pull
cd ../../..

echo "Fondamenti di Informatica"
cd "Triennale/PrimoAnno/Fondamenti di Informatica"
git pull
cd ../../..

echo "Architettura degli Elaboratori"
cd "Triennale/PrimoAnno/Architettura degli Elaboratori"
git pull
cd ../../..

echo "Technologies for Advanced Programming"
cd "Triennale/TerzoAnno/Technologies for Advanced Programming"
git pull
cd ../../..

